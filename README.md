# ci-collate

This module has been created while realizing we are doing the same or a very 
similar task from different tools. So, better if we provide a stand-alone tool
to do this task in a way we can use from multiple points as well as other 
projects may benefit.

[[_TOC_]]

## Usage

Use `ci-collate --help` for further information, but here you could see some 
examples:

### Single job request

Using the token stored in the environment variable `GITLAB_TOKEN`, one can 
request in the stdout the trace of a job. For example, the job's output of
`https://gitlab.freedesktop.org/gfx-ci-bot/mesa/-/jobs/37243489` can be 
requested by:
```commandline
ci-collate --namespace gfx-ci-bot job --trace 37243489
```
It didn't include `--project mesa` or 
`--gitlab-url https://gitlab.freedesktop.org/` because those are the default 
values.

The same example, but from a python code:

```python
from glcollate import Collate
Collate(namespace='gfx-ci-bot').from_job(37243489).trace()
```

To get the `failures.csv` file from this job:
```commandline
ci-collate --namespace gfx-ci-bot job --artifact results/failures.csv 37243489
```

And from python

```python
from glcollate import Collate
Collate(namespace='gfx-ci-bot').from_job(37243489).get_artifact('results/failures.csv')
```

### Pipeline request

Similar to `job` subcommand:

```commandline
ci-collate --namespace gfx-ci-bot pipeline --artifact results/failures.csv --status failed 819067
```

And from python:

```python
from glcollate import Collate
Collate(namespace='gfx-ci-bot').from_pipeline(819067).get_artifact('results/failures.csv', status='failed')
```

With pipelines some filtering can be setup base on _regex_, their _state_ or 
_status_.

```python
from pprint import pprint
from glcollate import Collate
pipeline = Collate(namespace='sergi', project='virglrenderer').from_pipeline(820983)
pprint(pipeline.get_artifact(artifact_name="results/junit.xml", job_regex=r"^piglit.*virt"))
```
Will return:
```
{'piglit-gl-virt 1/3': {37399191: '<?xml version="1.0" encoding="utf-8"?>\n'
                                  '<testsuites>\n'
                                  '  <testsuite id="0" name="gpu" '
                                  'package="testsuite/gpu" tests="0" '
                                  'errors="0" failures="0" '
                                  'hostname="localhost" '
                                  'timestamp="2023-03-03T09:58:19.215052909+00:00" '
                                  'time="0" />\n'
                                  '</testsuites>'},
 'piglit-gl-virt 2/3': {37399192: '<?xml version="1.0" encoding="utf-8"?>\n'
                                  '<testsuites>\n'
                                  '  <testsuite id="0" name="gpu" '
                                  'package="testsuite/gpu" tests="0" '
                                  'errors="0" failures="0" '
                                  'hostname="localhost" '
                                  'timestamp="2023-03-03T10:02:04.360507348+00:00" '
                                  'time="0" />\n'
                                  '</testsuites>'},
 'piglit-gl-virt 3/3': {37399193: '<?xml version="1.0" encoding="utf-8"?>\n'
                                  '<testsuites>\n'
                                  '  <testsuite id="0" name="gpu" '
                                  'package="testsuite/gpu" tests="0" '
                                  'errors="0" failures="0" '
                                  'hostname="localhost" '
                                  'timestamp="2023-03-03T09:59:44.446455139+00:00" '
                                  'time="0" />\n'
                                  '</testsuites>'},
 'piglit-gles-virt 1/3': {37399194: '<?xml version="1.0" encoding="utf-8"?>\n'
                                    '<testsuites>\n'
                                    '  <testsuite id="0" name="gpu" '
                                    'package="testsuite/gpu" tests="0" '
                                    'errors="0" failures="0" '
                                    'hostname="localhost" '
                                    'timestamp="2023-03-03T10:00:11.660667208+00:00" '
                                    'time="0" />\n'
                                    '</testsuites>'},
 'piglit-gles-virt 2/3': {37399195: '<?xml version="1.0" encoding="utf-8"?>\n'
                                    '<testsuites>\n'
                                    '  <testsuite id="0" name="gpu" '
                                    'package="testsuite/gpu" tests="0" '
                                    'errors="0" failures="0" '
                                    'hostname="localhost" '
                                    'timestamp="2023-03-03T10:02:39.617946666+00:00" '
                                    'time="0" />\n'
                                    '</testsuites>'},
 'piglit-gles-virt 3/3': {37399196: '<?xml version="1.0" encoding="utf-8"?>\n'
                                    '<testsuites>\n'
                                    '  <testsuite id="0" name="gpu" '
                                    'package="testsuite/gpu" tests="0" '
                                    'errors="0" failures="0" '
                                    'hostname="localhost" '
                                    'timestamp="2023-03-03T10:01:12.625760845+00:00" '
                                    'time="0" />\n'
                                    '</testsuites>'}}
```

In virglrenderer, some jobs store the artifacts in a subdirectory of `results` 
under the unsharded job name. It means the path contains a different string for 
each job, but it can be guessed by the tool.

If you look for the `failures.csv` artifacts for the `piglit` jobs in the 
pipeline 888148:

```bash
$ ci-collate --namespace sergi --project virglrenderer pipeline --artifact results/failures.csv --job-filter piglit-gl.* 888148
{'piglit-gl-host': {42259656: 'Artifact results/failures.csv not found',
                    42262859: 'Artifact results/failures.csv not found'},
 'piglit-gl-virt 1/3': {42259660: 'Artifact results/failures.csv not found'},
 'piglit-gl-virt 2/3': {42259661: 'spec@!opengl es '
                                  '3.0@gles-3.0-transform-feedback-uniform-buffer-object,UnexpectedPass\n',
                        42262857: 'spec@!opengl es '
                                  '3.0@gles-3.0-transform-feedback-uniform-buffer-object,UnexpectedPass\n'},
 'piglit-gl-virt 3/3': {42259662: 'spec@arb_fragment_program_shadow@tex-shadow2dnotdepth,Crash\n',
                        42262856: 'spec@arb_fragment_program_shadow@tex-shadow2dnotdepth,Crash\n'},
 'piglit-gles-host': {42259657: 'Artifact results/failures.csv not found',
                      42261524: 'Artifact results/failures.csv not found',
                      42262853: 'Artifact results/failures.csv not found'},
 'piglit-gles-virt 1/3': {42259663: 'Artifact results/failures.csv not found'},
 'piglit-gles-virt 2/3': {42259664: 'spec@!opengl es '
                                    '3.0@gles-3.0-transform-feedback-uniform-buffer-object,UnexpectedPass\n',
                          42262855: 'spec@!opengl es '
                                    '3.0@gles-3.0-transform-feedback-uniform-buffer-object,UnexpectedPass\n'},
 'piglit-gles-virt 3/3': {42259665: 'spec@arb_fragment_program_shadow@tex-shadow2dnotdepth,Crash\n',
                          42262854: 'spec@arb_fragment_program_shadow@tex-shadow2dnotdepth,Crash\n'}}
```

You have some of them `not found`. But the tool can be smart enough if you 
filter the artifact name by `results/*/failures.csv`:

```bash
$ ci-collate --namespace sergi --project virglrenderer pipeline --artifact results/*/failures.csv --job-filter piglit-gl.* 888148
{'piglit-gl-host': {42259656: 'spec@!opengl es '
                              '3.0@gles-3.0-transform-feedback-uniform-buffer-object,UnexpectedPass\n'
                              'spec@arb_fragment_program_shadow@tex-shadow2dnotdepth,Crash\n',
                    42262859: 'spec@!opengl es '
                              '3.0@gles-3.0-transform-feedback-uniform-buffer-object,UnexpectedPass\n'
                              'spec@arb_fragment_program_shadow@tex-shadow2dnotdepth,Crash\n'},
 'piglit-gl-virt 1/3': {42259660: 'Artifact results/*/failures.csv not found'},
 'piglit-gl-virt 2/3': {42259661: 'spec@!opengl es '
                                  '3.0@gles-3.0-transform-feedback-uniform-buffer-object,UnexpectedPass\n',
                        42262857: 'spec@!opengl es '
                                  '3.0@gles-3.0-transform-feedback-uniform-buffer-object,UnexpectedPass\n'},
 'piglit-gl-virt 3/3': {42259662: 'spec@arb_fragment_program_shadow@tex-shadow2dnotdepth,Crash\n',
                        42262856: 'spec@arb_fragment_program_shadow@tex-shadow2dnotdepth,Crash\n'},
 'piglit-gles-host': {42259657: 'Artifact results/*/failures.csv not found',
                      42261524: 'spec@!opengl es '
                                '3.0@gles-3.0-transform-feedback-uniform-buffer-object,UnexpectedPass\n'
                                'spec@arb_fragment_program_shadow@tex-shadow2dnotdepth,Crash\n',
                      42262853: 'spec@!opengl es '
                                '3.0@gles-3.0-transform-feedback-uniform-buffer-object,UnexpectedPass\n'
                                'spec@arb_fragment_program_shadow@tex-shadow2dnotdepth,Crash\n'},
 'piglit-gles-virt 1/3': {42259663: 'Artifact results/*/failures.csv not '
                                    'found'},
 'piglit-gles-virt 2/3': {42259664: 'spec@!opengl es '
                                    '3.0@gles-3.0-transform-feedback-uniform-buffer-object,UnexpectedPass\n',
                          42262855: 'spec@!opengl es '
                                    '3.0@gles-3.0-transform-feedback-uniform-buffer-object,UnexpectedPass\n'},
 'piglit-gles-virt 3/3': {42259665: 'spec@arb_fragment_program_shadow@tex-shadow2dnotdepth,Crash\n',
                          42262854: 'spec@arb_fragment_program_shadow@tex-shadow2dnotdepth,Crash\n'}}
```

In this output, the job `piglit-gles-host`, in the first attempt, didn't 
produce artifacts.

#### Information about job retries in pipelines

When one work with pipelines, there are jobs that could have been retried and 
the information from them could be the most interesting thing or extra info 
that confuses. So here has been established a way to differentiate if the user 
likes to work with or without this information.

When getting the `CollatePipeline` object one doesn't like to even collect this
information so don't gather this information from the jobs pipeline, they can 
call:

```python
from glcollate import Collate
Collate(namespace='gfx-ci-bot', project='virglrenderer').from_pipeline(833783, exclude_retried=True)
```

The default case, when this argument is not explicitly set to true, the 
`CollateJob` object in the `CollatePipeline` will store this information. If 
during the activity with the `CollatePipeline` object one doesn't like to 
interact with the retries, and only work with the last execution of the jobs, 
they can change the property `exclude_retried`.

To know if there are retried jobs in the pipeline, there is also a property 
listing them called `jobs_with_retries`.

So, for example, on this previous pipeline (building it with the default 
`exclude_retries` to `False`) we can access the set of jobs and there is one 
per job name but one can use them to see the retried instances:

```python
>>> pipeline.jobs(r"^piglit.*host")
 {CollateJob(piglit-gl-host), CollateJob(piglit-gles-host)}
>>> pipeline.jobs_with_retries
 {CollateJob(piglit-gles-host)}
>>> job = pipeline.jobs_with_retries.pop()
>>> job.ids
 [37399188, 37400671]
```

To get the trace from the retried job, one can specify it as an argument:
```python
print(job.trace(job_id=job.ids[0]))
```
When nothing is specified, it points to the last of the jobs.
