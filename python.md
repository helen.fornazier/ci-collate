# Python environment

## Python using virtualenv

Based on python 3.9, one can setup a virtual environments. 

```bash
$ pip3 -V
pip 20.3.4 from /usr/lib/python3/dist-packages/pip (python 3.9)
$ python3 -m pip install virtualenv
```

A virtual environment can be created in order to use this module: 

```bash
$ python3 -m virtualenv ci-collate.venv
$ source ci-collate.venv/bin/activate
(...)
$ python -m pip install python-gitlab ruamel.yaml tenacity
$ pip list
Package            Version
------------------ --------
certifi            2023.5.7
charset-normalizer 3.1.0
idna               3.4
pip                20.3.4
python-gitlab      3.14.0
requests           2.31.0
requests-toolbelt  1.0.0
ruamel.yaml        0.17.26
ruamel.yaml.clib   0.2.7
setuptools         44.1.1
tenacity           8.2.2
urllib3            2.0.2
wheel              0.34.2
```

To reproduce the same virtual environment that this tools is being tested:

```bash
$ pip install -r requirements.txt
```

### running tests

To run the pytests, one extra dependency is needed that is under development
```
$ pip install pytest 
$ pip install git+https://gitlab.freedesktop.org/sergi/python-gitlab-mock 
$ pip install -e . 
```

### Export changes on the environment

In case some newer packages are required, the easiest way to mark for others 
to use is:

```bash
pip freeze --all > requirements.txt
```

