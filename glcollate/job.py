#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from functools import cache
from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects.jobs import ProjectJob
from re import search as re_search
from tenacity import retry, stop_after_attempt, wait_exponential
from typing import Dict, Union


class CollateJob:
    # TODO: document methods
    __gl_job: Dict[int, ProjectJob] = None
    __latest_try: int = None

    def __init__(self, gitlab_job: ProjectJob):
        """
        Wrapper to a gitlab job with methods to manage the access to its trace
        and the artifacts.
        :param gitlab_job: gitlab job object to wrap
        """
        self.__gl_job = {gitlab_job.id: gitlab_job}
        self.__latest_try = gitlab_job.id

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"CollateJob({self})"

    @property
    def name(self):
        return self.__gl_job[self.__latest_try].name

    @property
    def name_unsharded(self) -> str:
        return self.__unshard_job_name()

    @property
    def id(self):
        return self.__gl_job[self.__latest_try].id

    @property
    def web_url(self):
        return self.__gl_job[self.__latest_try].web_url

    @property
    def stage(self):
        return self.__gl_job[self.__latest_try].stage

    @property
    def status(self):
        return self.__gl_job[self.__latest_try].status

    @property
    def attributes(self):
        return self.__gl_job[self.__latest_try].attributes

    @property
    def ids(self):
        ids = list(self.__gl_job.keys())
        ids.sort()
        return ids

    @property
    def has_retries(self) -> bool:
        return len(self.__gl_job) > 1

    def append_retry_job(self, job: ProjectJob) -> None:
        self.__gl_job[job.id] = job
        self.__latest_try = max(self.__gl_job.keys())

    def trace(self, job_id: int = None) -> str:
        """
        Query the traces log of the job.
        :param job_id:
        :return: string with the job trace log
        """
        if job_id is not None and job_id not in self.ids:
            raise KeyError
        if trace := self.__get_trace(job_id):
            return trace
        raise FileNotFoundError("Trace not found")

    # TODO: list the available artifacts

    def get_artifact(self, artifact_name, job_id: int = None) -> str:
        """
        Query the artifact file of the job.
        :param artifact_name:
        :param job_id:
        :return: string with the content of the file interpreted as string
        """
        if job_id is not None and job_id not in self.ids:
            raise KeyError(f"Job {job_id} is not in {self.ids}")
        if artifact := self.__get_artifact(artifact_name, job_id):
            return artifact
        raise FileNotFoundError(f"Artifact {artifact_name} not found")

    @cache
    @retry(stop=stop_after_attempt(6), wait=wait_exponential(multiplier=1))
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def __get_trace(self, job_id: int = None) -> Union[str, None]:
        try:
            job_id = self.__latest_try if job_id is None else job_id
            return self.__gl_job[job_id].trace().decode("UTF-8")
        except GitlabGetError as exception:
            if exception.response_code == 404:
                return None

    @cache
    @retry(stop=stop_after_attempt(6), wait=wait_exponential(multiplier=1))
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def __get_artifact(self, artifact_pattern, job_id: int = None) -> Union[str, None]:
        job_id = self.__latest_try if job_id is None else job_id
        artifact = None
        for artifact_name in self.__path_pattern(artifact_pattern):
            try:
                artifact = self.__gl_job[job_id].artifact(artifact_name).decode("UTF-8")
            except GitlabGetError as exception:
                code = exception.response_code
                if code == 404:
                    # go to check the next path with the generator
                    continue
                raise exception
            else:
                break
        return artifact

    def __unshard_job_name(self) -> str:
        """
        Remove the suffix on the sharded jobs to have a single name for all of them.
        :return: original name before sharding
        """
        search = re_search(r"(.*) [0-9]+/[0-9]+$", self.name)
        if search:  # if it has a " n/m" at the end, remove it.
            return search.group(1)
        return self.name

    def __path_pattern(self, artifact_name) -> str:
        if artifact_name.count("/*/"):
            yield artifact_name.replace("/*/", f"/{self.name_unsharded}/")
            yield artifact_name.replace("/*/", f"/")
        else:
            yield artifact_name
